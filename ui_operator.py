from tkinter import Label, Frame, Button, Listbox, Text, Grid, Tk, ACTIVE, END, N, E, S, W, Entry, SW, NW, NE, SE
from tkinter.ttk import Combobox
from ui_forms import Forms
from datetime import date
from utils_Enums import Tables

class Frm_Operator(Forms):

    def __init__(self, db):
        self.db = db
        self._create_form()

    def _set_hood(self):
        self.db.current_hood = self.cb_hoods.get()
        self.lbl_active_hood['text'] = self.db.current_hood
        self.btn_add_entry.configure(state='active')
        self.btn_remove_entry.configure(state='active')
        self.btn_submit.configure(state='active')
        self.cb_prefix.configure(state='readonly')
        self.txt_comments.configure(state='normal')
        self.txt_quantity.configure(state='normal')

    def _add_entry(self):
        new_entry = (
            self.db.current_operator[1],
            self.db.current_hood,
            self.lbl_date['text'],
            self.cb_prefix.get(),
            self.txt_quantity.get(),
            self.txt_comments.get()
        )
        print(new_entry)
        if self._check_if_duplicate_entry(self.lb_entries, new_entry) == True:
            self._spawn_dialog("Error - Duplicate Entry", "You have already added this entry.\nPlease review your entry and try again.")
        else:
            self.db.current_entries.append(new_entry)
            self._update_listbox(self.lb_entries, self.db.current_entries)

    def _remove_entry(self):
        index = self.lb_entries.index(ACTIVE)
        self.lb_entries.delete(index)
        del self.db.current_entries[index]
        self._update_listbox(self.lb_entries, self.db.current_entries)

    def _submit_entries(self):
        for i in range(len(self.db.current_entries)):
            self.db.save_entry(
                self.db.current_entries[i][0],
                self.db.current_entries[i][1],
                self.db.current_entries[i][3],
                self.db.current_entries[i][4],
                self.db.current_entries[i][5]
            )
        self.lb_entries.delete(0, END)
        self.db.current_entries = []
        self._update_submitted_entries()

    def _update_submitted_entries(self):
        self.lb_submitted_entries.delete(0, END)
        for item in self.db.get_entries(operator=self.db.current_operator[1], date=self.lbl_date['text']):
            self.lb_submitted_entries.insert(END, f"[{item[3]}] [{item[4]}] @ [{item[1]}] [{item[5]}]")

    def _check_if_duplicate_entry(self, listbox, new_entry):
        for row in listbox.get(0, END):
            if f"[{new_entry[3]}] [{new_entry[4]}] [{new_entry[5]}]" == row:
                return True
        else:
            return False

    def _update_listbox(self, listbox, new_list):
        listbox.delete(0, END)
        for entry in new_list:
            listbox.insert(END, f"[{entry[3]}] [{entry[4]}] [{entry[5]}]")
        self.Window.update()

    def _create_form(self):
        self.Window = Tk()
        self.Window.configure(bg="RoyalBlue1")
        self.Window.title(f"Daily Input Log -- {self.db.current_operator[1]}")
        self.Window.geometry("%dx%d+%d+%d" % self._get_form_geometry(855, 565))

        ### FRAMES ###
        self.Header = Frame(self.Window, bg='RoyalBlue1', bd=5, width=1200, height=240)
        self.Header.grid(column=0, row=0, padx=0, pady=0, sticky=NW)

        self.Entries = Frame(self.Window, bg='RoyalBlue1', bd=5, width=1200, height=240)
        self.Entries.grid(column=0, row=1, padx=0, pady=5, sticky=NW)

        self.Display_Entries = Frame(self.Window, bg='RoyalBlue1', bd=5, width=800, height=240)
        self.Display_Entries.grid(column=0, row=1, padx=0, pady=5, sticky=NE)

        self.All_Entries_Today = Frame(self.Window, bg='RoyalBlue1', bd=5, width=400, height=240)
        self.All_Entries_Today.grid(column=0, row=1, padx=0, pady=5, sticky=SW)

        ### HEADER WIDGETS ####
        self.lbl_operator_header = Label(self.Header, text="Current Operator:", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.lbl_operator_header.grid(column=0, row=0, padx=20, pady=5, sticky=W)

        self.lbl_date_header = Label(self.Header, text="Current Date:", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.lbl_date_header.grid(column=1, row=0, padx=20, pady=5, sticky=W)

        self.lbl_hood_header = Label(self.Header, text="Select Active Hood", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.lbl_hood_header.grid(column=2, row=0, padx=20, pady=5, sticky=W)

        self.active_hood_header = Label(self.Header, text="Current Active Hood", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.active_hood_header.grid(column=3, row=0, padx=20, pady=5, sticky=W)
        
        self.lbl_current_operator = Label(self.Header, text=f"{self.db.current_operator[1]}", font=('Arial', 12), bg='RoyalBlue1')
        self.lbl_current_operator.grid(column=0, row=1, padx=20, pady=5, sticky=W)

        self.lbl_date = Label(self.Header, text=f"{str(date.today())}", font=('Arial', 12), bg='RoyalBlue1')
        self.lbl_date.grid(column=1, row=1, padx=20, pady=5, sticky=W)

        self.cb_hoods = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly')
        self.cb_hoods['values'] = [ row[1] for row in self.db.hoods ]
        self.cb_hoods.grid(column=2, row=1, padx=20, pady=5, sticky=E)

        self.lbl_active_hood = Label(self.Header, text="<Select Hood>", font=('Arial', 12), bg='RoyalBlue1')
        self.lbl_active_hood.grid(column=3, row=1, padx=20, pady=5, sticky=E)

        self.lbl_operator_shift = Label(self.Header, text=f"Operator Shift: {self.db.current_operator[2]}", font=('Arial', 14), bg='RoyalBlue1')
        self.lbl_operator_shift.grid(column=1, row=2, padx=20, pady=5, sticky=SE)

        self.btn_set_hood = Button(self.Header, text="Select Hood", bg='blue', command=self._set_hood)
        self.btn_set_hood.grid(column=2, row=2, padx=20, pady=5, sticky=E)

        def _logout():
            from ui_login import LoginForm
            self.Window.destroy()
            LoginForm(self.db)

        self.btn_logout = Button(self.Header, text="Logout", bg='orange', command=_logout)
        self.btn_logout.grid(column=0, row=2, padx=20, pady=5, sticky=SE)

        def _changepw():
            from ui_changepw import ChangePWForm
            ChangePWForm(self.db.current_operator[1], self.db)

        self.btn_changepw = Button(self.Header, text="Change PW", bg='green', command=_changepw)
        self.btn_changepw.grid(column=0, row=2, padx=20, pady=5, sticky=SW)


        ### ENTRIES WIDGETS ###
        self.lbl_prefix_header = Label(self.Entries, text="Select a Prefix: ", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.lbl_prefix_header.grid(column=0, row=5, padx=20, pady=10, sticky=W)

        self.cb_prefix = Combobox(self.Entries, font=('Arial', 10), width=6, state='disabled')
        self.cb_prefix['values'] = [ row[1] for row in self.db.prefixes ]
        self.cb_prefix.grid(column=0, row=6, padx=20, pady=5, sticky=W)

        self.lbl_comments = Label(self.Entries, text="Comments:", font=('Arial', 14), bg='RoyalBlue1')
        self.lbl_comments.grid(column=0, row=7, padx=20, pady=10, sticky=W)

        self.txt_comments = Entry(self.Entries, width=32, state='disabled')
        self.txt_comments.grid(column=0, row=8, padx=20, pady=5, sticky=W)

        self.lbl_quantity = Label(self.Entries, text="Quantity:", font=('Arial', 14), bg='RoyalBlue1')
        self.lbl_quantity.grid(column=1, row=5, padx=20, pady=10, sticky=E)

        self.txt_quantity = Entry(self.Entries, width=8, state='disabled')
        self.txt_quantity.grid(column=1, row=6, padx=20, pady=5, sticky=E)

        self.btn_add_entry = Button(self.Entries, text="Add >>", state='disabled', bg='cyan2', command=self._add_entry)
        self.btn_add_entry.grid(column=1, row=7, padx=20, pady=5, sticky=SE)

        self.btn_remove_entry = Button(self.Entries, text="<< Remove", state='disabled', bg='red2', command=self._remove_entry)
        self.btn_remove_entry.grid(column=1, row=8, padx=20, pady=5, sticky=SE)

        ### DISPLAY_ENTRIES WIDGETS ###
        self.lbl_display = Label(self.Display_Entries, text="Current Entries:", font=('Arial', 14), bg='RoyalBlue1')
        self.lbl_display.grid(column=0, row=0, padx=20, pady=10, sticky=NW)

        self.lb_entries = Listbox(self.Display_Entries, width=70, height=20, bg='gray66', selectmode='SINGLE')
        self.lb_entries.grid(column=0, row=1, padx=20, pady=10, sticky=NW)

        self.btn_submit = Button(self.Display_Entries, text="Submit", state='disabled', bg='cyan2', command=self._submit_entries)
        self.btn_submit.grid(column=0, row=2, padx=20, pady=5, sticky=SE)

        def _open_records():
            from ui_reports import Frm_Records
            Frm_Records(self.db)
        
        self.btn_reports = Button(self.Display_Entries, text="View Historical Records", state='active', bg='cyan2', command=_open_records)
        self.btn_reports.grid(column=0, row=2, padx=20, pady=5, sticky=SW)

        ### ALL ENTRIES FROM TODAY WIDGETS ###
        self.lbl_submitted_entries = Label(self.All_Entries_Today, text="All Entries Today", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.lbl_submitted_entries.grid(column=0, row=0, padx=20, pady=5, sticky=NW)

        self.lb_submitted_entries = Listbox(self.All_Entries_Today, width=55, height=12, bg='gray66')
        self.lb_submitted_entries.grid(column=0, row=1, padx=10, pady=5, sticky=SW)
        self._update_submitted_entries()

        self.Window.mainloop()
