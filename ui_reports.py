from tkinter import Label, Frame, Button, Listbox, Text, Grid, Tk, ACTIVE, END, N, E, S, W, Entry, SW, NW, NE, SE
from tkinter.ttk import Combobox
from ui_forms import Forms
from datetime import date, time, timedelta

class Frm_Records(Forms):
    
    def __init__(self, db):
        self.db = db
        self.records = []
        self.column_count = 0
        self.record_count = 0
        self._create_form()

    def _create_form(self):
        self.Window = Tk()
        self.Window.title(f"Daily Output Log - Reports")
        self.Window.configure(bg="RoyalBlue1")
        self.Window.geometry("%dx%d+%d+%d" % self._get_form_geometry(845, 834))

        self.Header = Frame(self.Window, bg='RoyalBlue1', bd=5, width=800, height=240)
        self.Header.grid(column=0, row=0, padx=1, pady=1, sticky=NW)

        self.Display_Retrieved = Frame(self.Window, bg='RoyalBlue1', bd=5, width=800, height=600)
        self.Display_Retrieved.grid(column=0, row=1, padx=1, pady=1, sticky=NW)

        # Header Frame

        self.lbl_date_start = Label(self.Header, text="Choose Start Date:", font=('Arial Bold', 12), bg='RoyalBlue1')
        self.lbl_date_start.grid(column=0, row=0, padx=5, pady=5, sticky=NW)

        self.lbl_date_end = Label(self.Header, text="Choose End Date\n(Default Today):", font=('Arial Bold', 12), bg='RoyalBlue1')
        self.lbl_date_end.grid(column=0, row=1, padx=5, pady=5, sticky=NW)

        days = [ str(x) for x in range(1, 32)]
        months = [ str(x) for x in range(1, 13)]
        years = ['', '2019']
        prefixes = sorted([ row[1] for row in self.db.prefixes ])
        months.insert(0, '')
        days.insert(0, '')
        prefixes.insert(0, '')

        self.cb_date_year = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly' )
        self.cb_date_year['values'] = years
        self.cb_date_year.grid(column=1, row=0, padx=5, pady=5, sticky=NW)

        self.cb_date_month = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly' )
        self.cb_date_month['values'] = months
        self.cb_date_month.grid(column=2, row=0, padx=5, pady=5, sticky=NW)

        self.cb_date_days = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly' )
        self.cb_date_days['values'] = days
        self.cb_date_days.grid(column=3, row=0, padx=5, pady=5, sticky=NW)

        self.cb_date_year_end = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly' )
        self.cb_date_year_end['values'] = years
        self.cb_date_year_end.grid(column=1, row=1, padx=5, pady=5, sticky=NW)

        self.cb_date_month_end = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly' )
        self.cb_date_month_end['values'] = months
        self.cb_date_month_end.grid(column=2, row=1, padx=5, pady=5, sticky=NW)

        self.cb_date_days_end = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly' )
        self.cb_date_days_end['values'] = days
        self.cb_date_days_end.grid(column=3, row=1, padx=5, pady=5, sticky=NW)

        self.lbl_prefix = Label(self.Header, text="Choose a Prefix (Optional)", font=('Arial Bold', 12), bg='RoyalBlue1')
        self.lbl_prefix.grid(column=4, row=0, padx=5, pady=5, sticky=NW)

        self.cb_prefixes = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly')
        self.cb_prefixes['values'] = prefixes
        self.cb_prefixes.grid(column=4, row=1, padx=5, pady=5, sticky=NW)

        self.cb_operators = Combobox(self.Header, font=('Arial', 10), width=6, state='readonly')
        if self.db.current_operator[3] == 3:
            self.cb_operators['values'] = sorted([ row[1] for row in self.db.operators ])
        elif self.db.current_operator[3] == 2:
            self.cb_operators['values'] = sorted([ row[1] for row in self.db.operators if row[2] == self.db.current_operator[2] ])
        elif self.db.current_operator[3] == 1:
            self.cb_operators.set(self.db.current_operator[1])
            self.cb_operators.configure(state='disabled')
        self.cb_operators.grid(column=0, row=2, padx=5, pady=5, sticky=NW)

        self.lbl_records_total = Label(self.Header, text=f"Total Records: \t{self.db.number_of_records(self.cb_operators.get())} / {self.db.number_of_records()}", font=('Arial', 12), bg='RoyalBlue1')
        self.lbl_records_total.grid(column=5, row=0, padx=5, pady=5, sticky=NW)

        self.lbl_column_count_total = Label(self.Header, text=f"Total Columns: \t{self.db.total_columns_loaded_by_operator(self.cb_operators.get())}", font=('Arial', 12), bg='RoyalBlue1')
        self.lbl_column_count_total.grid(column=5, row=1, padx=5, pady=5, sticky=NW)

        def _clear():
            self.cb_date_year.set('')
            self.cb_date_days.set('')
            self.cb_date_month.set('')
            self.cb_date_year_end.set('')
            self.cb_date_month_end.set('')
            self.cb_date_days_end.set('')
            self.cb_prefixes.set('')
            if self.db.current_operator[3] != 1:
                self.cb_operators.set('')

        def _set_today():
            current_date = _date_as_list(str(date.today()))
            self.cb_date_year.set(current_date[0])
            self.cb_date_month.set(current_date[1])
            self.cb_date_days.set(current_date[2])

        def _set_week():
            current_date = _date_as_list(str(date.today()))
            self.cb_date_days_end.set(current_date[0])
            self.cb_date_month_end.set(current_date[1])
            self.cb_date_days_end.set(current_date[2])

            start_date = _date_as_list(str(date.today() - timedelta(days=7)))
            self.cb_date_year.set(current_date[0])
            self.cb_date_month.set(start_date[1])
            self.cb_date_days.set(start_date[2])

        def _set_month():
            current_date = _date_as_list(str(date.today()))
            self.cb_date_days_end.set(current_date[0])
            self.cb_date_month_end.set(current_date[1])
            self.cb_date_days_end.set(current_date[2])

            start_date = _date_as_list(str(date.today() - timedelta(days=30)))
            self.cb_date_year.set(current_date[0])
            self.cb_date_month.set(start_date[1])
            self.cb_date_days.set(start_date[2])

        self.btn_set_today = Button(self.Header, text="Today", state='active', bg='green2', command=_set_today)
        self.btn_set_today.grid(column=1, row=2, padx=5, pady=5, sticky=NW)

        self.btn_set_week = Button(self.Header, text="7 Days", state='active', bg='orange2', command=_set_week)
        self.btn_set_week.grid(column=2, row=2, padx=5, pady=5, sticky=NW)

        self.btn_set_month = Button(self.Header, text="30 Days", state='active', bg='pink2', command=_set_month)
        self.btn_set_month.grid(column=3, row=2, padx=5, pady=5, sticky=NW)

        self.btn_clear = Button(self.Header, text="Clear", state='active', bg='firebrick1', command=_clear)
        self.btn_clear.grid(column=4, row=2, padx=5, pady=5, sticky=NW)

        ## Display Retrieved Frame ##
        
        def _get_date_as_string(year, month, day):
            if year != '' and month != '' and day != '':
                return f"{year}-{month}-{day}"
            else:
                return None

        def _get_record_stats():
            count = 0 
            for row in self.records:
                count += row[4]
            return (count, len(self.records))
        
        def _date_as_list(date):
                return [int(x) for x in date.split('-')]

        def _get_records():
            self.lb_retrieved.delete(0, END)
            
            operator = self.cb_operators.get()
            if operator == '':
                operator = None
            
            start_date = _get_date_as_string(self.cb_date_year.get(), self.cb_date_month.get(), self.cb_date_days.get())
            end_date = _get_date_as_string(self.cb_date_year_end.get(), self.cb_date_month_end.get(), self.cb_date_days_end.get())
            
            if start_date == None:
                self.records = self.db.get_entries(operator=operator, prefix=self.cb_prefixes.get())
            elif start_date != None and end_date == None:
                self.records = self.db.get_entries(operator=operator, date=start_date, prefix=self.cb_prefixes.get())
            else:
                self.records = self.db.get_entries(operator=operator, start_date=start_date, end_date=end_date, prefix=self.cb_prefixes.get())

            for entry in self.records:
                self.lb_retrieved.insert(END, f"{entry[0]}     {entry[1]}     {entry[2]}     {entry[3]}     {entry[4]}     {entry[5]}")

            self.column_count = _get_record_stats()[0]
            self.record_count = _get_record_stats()[1]
            self.lbl_entries.configure(text=f"Entries Found:\tColumns: {self.column_count}\tRecords: {self.record_count}")
            self.lbl_records_total.configure(text=f"Total Records: \t{self.db.number_of_records(self.cb_operators.get())} / {self.db.number_of_records()}")
            self.lbl_column_count_total.configure( text=f"Total Columns: \t{self.db.total_columns_loaded_by_operator(self.cb_operators.get())}")
            self.Window.update()

        self.btn_get_records = Button(self.Header, text="Find Records", state='active', bg='purple1', command=_get_records)
        self.btn_get_records.grid(column=4, row=1, padx=5, pady=5, sticky=NE)

        self.lbl_entries = Label(self.Display_Retrieved, text=f"Entries Found:", font=('Arial Bold', 14), bg='RoyalBlue1')
        self.lbl_entries.grid(column=0, row=0, padx=5, pady=5, sticky=NW)

        self.lb_retrieved = Listbox(self.Display_Retrieved, width=137, height=40, bg='gray66', selectmode='SINGLE')
        self.lb_retrieved.grid(column=0, row=1, padx=5, pady=5, sticky=NW)