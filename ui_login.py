from tkinter import Label, Frame, Button, Listbox, Text, Grid, Tk, ACTIVE, END, N, E, S, W, Entry, SW, NW, NE, SE
from tkinter.ttk import Combobox
from ui_forms import Forms

class LoginForm(Forms):

    def __init__(self, database):
        self.login_form(database)

    def login_form(self, db):
        self.window = Tk()
        self.window.title("Daily Output Log - Login")
        self.window.configure(bg="RoyalBlue1")
        self.window.geometry("%dx%d+%d+%d" % self._get_form_geometry(300, 90))

        self.Login = Frame(self.window, bd=5, bg="RoyalBlue1")
        self.Login.grid(column=0, row=0, sticky=W)
        
        self.lbl_username = Label(self.Login, text="Username", font=('Arial', 12), bg="RoyalBlue1")
        self.lbl_username.grid(column=0, row=0, padx=2.5, pady=2.5, stick=W)
        
        self.lbl_password = Label(self.Login, text="Password", font=('Arial', 12), bg="RoyalBlue1")
        self.lbl_password.grid(column=0, row=1, padx=2.5, pady=2.5, stick=W)
        
        self.cb_usernames = Combobox(self.Login, font=('Arial', 10), width=8, state='readonly')
        self.cb_usernames['values'] = sorted([ row[1] for row in db.operators ])
        self.cb_usernames.grid(column=1, row=0, padx=2.5, pady=2.5,  stick=E)
        
        self.entry_password = Entry(self.Login, width=16, show="*")
        self.entry_password.grid(column=1, row=1, padx=2.5, pady=2.5,  stick=E)

        def _login():
            from ui_operator import Frm_Operator
            from db_Operator import Operator
            from ui_changepw import ChangePWForm
            username = self.cb_usernames.get()
            if username != None:
                if self.entry_password.get() == "password" and Operator.check_password(db, username, "password") == True:
                    ChangePWForm(username=username, database=db)
                if Operator.check_password(db, username, self.entry_password.get()) == True:
                    for row in db.operators:
                        if row[1] == username:
                            db.current_operator = row
                            break
                    self.window.destroy()
                    if db.current_operator[3] == 1 or db.current_operator[3] == 2:     #   Standard Operator (1) or Lead (2)
                        Frm_Operator(db)
                    elif db.current_operator[3] == 3:   #   Supervisor (3)
                        from ui_reports import Frm_Records
                        Frm_Records(db)
                else:
                    self._spawn_dialog("Error!", "Password was incorrect. Please try again.")

        def _keypress(event):
            _login()

        self.window.bind('<Return>', _keypress)
        
        self.btn_login = Button(self.Login, text="Log In", font=('Arial', 12), bg='blue', command=_login)
        self.btn_login.grid(column=2, row=0, padx=2.5, pady=2.5, stick=SW)

        self.window.mainloop()