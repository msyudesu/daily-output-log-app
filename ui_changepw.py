from tkinter import Label, Frame, Button, Listbox, Text, Grid, Tk, ACTIVE, END, N, E, S, W, Entry, SW, NW, NE, SE
from tkinter.ttk import Combobox
from ui_forms import Forms

class ChangePWForm(Forms):

    def __init__(self, username, database):
        self.username = username
        self.change_pw_form(database)

    def change_pw_form(self, db):
        self.window = Tk()
        self.window.title("Change Password")
        self.window.geometry("%dx%d+%d+%d" % self._get_form_geometry(315, 150))

        self.Main = Frame(self.window, bd=5)
        self.Main.grid(column=0, row=0, sticky=NW)

        self.lbl_old_pw = Label(self.Main, text="Old Password", font=('Arial', 12))
        self.lbl_old_pw.grid(column=0, row=0, padx=5, pady=5, sticky=NW)

        self.lbl_new_password = Label(self.Main, text="New Password" , font=('Arial', 12))
        self.lbl_new_password.grid(column=0, row=1, padx=5, pady=5, sticky=NW)

        self.lbl_pw_check = Label(self.Main, text="Retype Password", font=('Arial', 12))
        self.lbl_pw_check.grid(column=0, row=2, padx=5, pady=5, sticky=NW)

        self.old_pw = Entry(self.Main, width=16, show="*")
        self.old_pw.grid(column=1, row=0, padx=5, pady=5, sticky=NE)
        
        self.new_pw = Entry(self.Main, width=16, show="*")
        self.new_pw.grid(column=1, row=1, padx=5, pady=5, sticky=NE)

        self.check_pw = Entry(self.Main, width=16, show="*")
        self.check_pw.grid(column=1, row=2, padx=5, pady=5, sticky=NE)

        def _change_pw():
            from db_Operator import Operator
            if Operator.check_password(db, self.username, self.old_pw.get()) == True:
                if self.new_pw.get() == self.check_pw.get():
                    Operator.change_password(db, self.username, self.new_pw.get())
                    self.window.destroy()
                else:
                    self.new_pw.configure(text='')
                    self.check_pw.configure(text='')
                    self._spawn_dialog("Error", "Password didn't match, please try again.")
                    

        self.btn_change = Button(self.Main, text="Change Password", font=('Arial', 12), bg='green1', command=_change_pw)
        self.btn_change.grid(column=0, row=3, padx=5, pady=5, sticky=NW)
