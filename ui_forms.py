#from tkinter import *
from tkinter import Label, Frame, Button, Listbox, Text, Grid, Tk, ACTIVE, END, N, E, S, W, Entry, SW, NW, NE, SE
from tkinter.ttk import Combobox

class Forms():

    def _spawn_dialog(self, title, message):
        self.dialog = Tk()
        self.dialog.title(f"{title}")
        self.dialog.geometry("440x100")
        self.lbl_dialog = Label(self.dialog, text=f"{message}", font=('Arial Bold', 14), fg='red')
        self.lbl_dialog.grid(column=0,row=0, padx=10, pady=10, sticky=N)
        def _close():
            self.dialog.destroy()
        self.btn_ok = Button(self.dialog, text="[ OK ]", bg='white', command=_close)
        self.btn_ok.grid(column=0, row=1, padx=10, pady=10, stick=N)
        self.dialog.mainloop()

    def _get_form_geometry(self, height, width):        
        #   Just open it in the top left corner for now.
        xpos = 0
        ypos = 0
        return (height, width, xpos, ypos)