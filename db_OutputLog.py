import pyodbc
from db_Prefix import Prefix
from db_Hoods import Hood
from db_Operator import Operator
from utils_Enums import Tables


class Output_Log():

    def __init__(self, mode="test"):
        self.mode = mode
        self._start_connection()
        self.operators = Operator.fetch_all_operators(self)
        self.prefixes = Prefix.fetch_all_prefixes(self)
        self.hoods = Hood.fetch_all_hoods(self)
        self.current_entries = []
        self.current_hood = None
        self.current_operator = ()

    #   Kept for references
    def _start_connection(self):
        try:
            if self.mode.lower() == "live":
                self.conn = pyodbc.connect(
                    "Driver={SQL Server};"
                    "Server={NPT-DEVPC01};"
                    "Database={column_outputdb};"
                    "MARS_Connection={yes};"
                    "Trusted_Connection={yes};",
                    autocommit=False
                )
            elif self.mode.lower() == "test":
                self.conn = pyodbc.connect(
                    "Driver={SQL Server};"
                    "Server={5CG8471S8Q};"
                    "Database={column_outputdb};"
                    "MARS_Connection={yes};"
                    "Trusted_Connection={yes};",
                    autocommit=False
                )
            print("Connection successful")
        except pyodbc.DatabaseError:
            print("Failed to connect")

    def close_connection(self):
        self.conn.close()

    def translate_ID(self, table_id, value):
        tables = [self.operators, self.prefixes, self.hoods]
        if value != None:
            if isinstance(value, str):              #   Change ID into name.
                current = 1
                change_to = 0
            elif isinstance(value, int):            #   Change name into ID.
                current = 0
                change_to = 1
            for row in tables[table_id]:
                if row[current] == value:
                    return row[change_to]       

    def number_of_records(self, operator=None):
        with self.conn:
            cursor = self.conn.cursor()
            if operator == None:
                cursor.execute("select count(*) from dbo.column_output")
            else:
                cursor.execute("select count(*) from dbo.column_output where Operator = ?", self.translate_ID(0, operator))
            count = cursor.fetchone()[0]
        return count

    def get_entries(self, operator=None, date=None, start_date=None, end_date=None, prefix=None):
        with self.conn:
            cursor = self.conn.cursor()
            if end_date == None:
                if start_date != None:
                    date = start_date
                argsin = [self.translate_ID(0, operator), date, self.translate_ID(1, prefix)]
                arg_sql = ["Operator = ?", "EntryDate = ?", "Prefix = ?"]
            else:
                argsin = [self.translate_ID(0, operator), self.translate_ID(1, prefix), start_date, end_date]
                arg_sql = ["Operator = ?", "Prefix = ?", "EntryDate BETWEEN ?", "?"]

            sql = "select * from dbo.column_output"
            params = []

            for i in range(len(argsin)):
                if argsin[i] != None:
                    sql += " WHERE "
                    for i in range(len(argsin)):
                        if argsin[i] != None:
                            if sql[-7:] != " WHERE ":
                                sql += " AND "
                            sql += arg_sql[i]
                            params.append(argsin[i])
                    break
                
            if params == None:
                print(sql)
                raw_entries = cursor.execute(sql)
            else:
                print(sql)
                raw_entries = cursor.execute(sql, tuple(params))
            
            entries = []
            for row in raw_entries:
                entries.append(
                    [
                        self.translate_ID(0, row[1]),   #   Operator
                        self.translate_ID(2, row[2]),   #   Hood
                        row[3],                         #   Date
                        self.translate_ID(1, row[4]),   #   Prefix
                        row[5],                         #   Quantity
                        row[6]                          #   Comments
                    ]
                )
            return entries

    def total_columns_loaded_by_operator(self, operator):
        with self.conn:
            cursor = self.conn.cursor()
            cursor.execute("select sum(Quantity) from dbo.column_output where Operator = ?", self.translate_ID(0, operator))
            total = cursor.fetchone()[0]
        return total

    def save_entry(self, operator, hood, prefix, quantity, comments=None):
        cursor = self.conn.cursor()
        try:
            cursor.execute("exec dbo.AddEntry @Operator = ?, @Hood = ?, @Prefix = ?, @Quantity = ?, @Comments = ?", 
                self.translate_ID(0, operator), 
                self.translate_ID(2, hood), 
                self.translate_ID(1, prefix), 
                quantity, comments)
        except pyodbc.IntegrityError:
            print("Failed to upload record.")
            self.conn.rollback()
        except Exception:
            print("An unknown error occured.")
            self.conn.rollback()
        else:
            print(f"Succes! Added Entry:  {prefix} {quantity} @ {hood} by {operator}")
            self.conn.commit()