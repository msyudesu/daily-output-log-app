import pyodbc

class Hood():

    @staticmethod
    def create_hood(connection, hood):
        cursor = connection.conn.cursor()
        try:
            cursor.execute("insert into dbo.hoods values(?)", hood)
        except pyodbc.IntegrityError:
            connection.conn.rollback()
        except Exception:
            connection.conn.rollback()
        else:
            connection.conn.commit()

    @staticmethod
    def modify_hood(connection, hood, new_hood):
        cursor = connection.conn.cursor()

        try:
            cursor.execute("update dbo.hoods set Hood = ? where Hood = ?", hood, new_hood)
        except pyodbc.IntegrityError:
            connection.conn.rollback()
        except Exception:
            connection.conn.rollback()
        else:
            connection.conn.commit()

    @staticmethod
    def fetch_all_hoods(connection):
        with connection.conn:
            cursor = connection.conn.cursor()
            query = cursor.execute("select HoodID, Hood from dbo.hoods")
            entries = [row for row in query]
        return entries