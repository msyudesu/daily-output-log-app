from enum import Enum, unique

@unique
class Tables(Enum):
    operators = 0,
    prefixes = 1,
    hoods = 2