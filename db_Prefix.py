import pyodbc

class Prefix():

    @staticmethod
    def create_prefix(connection, prefix):
        cursor = connection.conn.cursor()
        try:
            cursor.execute("insert into dbo.column_prefix values (?)", prefix)
        except Exception as e:
            connection.conn.rollback()
            print(e)
        else:
            print(f"{prefix} added to Prefixes")
            connection.conn.commit()
    
    @staticmethod
    def modify_prefix(connection, prefix, new_prefix):
        cursor = connection.conn.cursor()
        try:
            cursor.execute("update dbo.column_prefix set Prefix = ? where Prefix = ?", prefix, new_prefix)
        except Exception as e:
            connection.conn.rollback()
            print(e)
        else:
            connection.conn.commit()

    @staticmethod
    def fetch_all_prefixes(connection):
        '''
        Returns a list of all rows in table dbo.column_prefix.\n
        Access columns by the associated index below:\n
        1)  PrefixID
        2)  Prefix
        '''

        with connection.conn:
            cursor = connection.conn.cursor()
            query = cursor.execute("select * from dbo.column_prefix")
            prefixes = [ row for row in query]
        return prefixes