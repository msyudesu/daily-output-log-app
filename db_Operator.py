import pyodbc

class Operator():

    @staticmethod
    def _get_hash(password):
        import hashlib
        hashed = hashlib.sha256(password.encode())
        return hashed.hexdigest()

    @staticmethod
    def create_operator(connection, operator, shift, role, password):
        cursor = connection.conn.cursor()

        if len(operator) < 2:
            raise Exception("Operator Name must be at least 2 characters!")

        if shift != 1 and shift != 2 and shift != 3:
            raise Exception("Invalid Shift!\nValid shifts are:\n1, 2, 3")
        
        if role != 1 and role != 2 and role != 3:
            raise Exception("Invalid Role!\nValid roles are:\n\t1=Supervisor\n\t2=Lead\n\t3=Standard Operator.")

        if len(password) <= 4:
            raise Exception("Password must be longer than 4 characters!")

        password = Operator._get_hash(password)
        
        try:
            cursor.execute("insert into dbo.operators values (?, ?, ?, ?)", operator, shift, role, str(password))
        except pyodbc.IntegrityError:
            print("Operator already exists!")
        except Exception as e:
            print(e)
            connection.conn.rollback()
        else:
            print(f"Operation succesful - {operator} was added.")
            connection.conn.commit()

    @staticmethod
    def change_password(connection, operator, password):
        cursor = connection.conn.cursor()
        if len(password) <= 4:
            raise Exception("Password must be longer than 4 characters!")

        try:
            cursor.execute("update dbo.operators set Op_PW = ? where Op_username = ?", str(Operator._get_hash(password)), operator)        
        except Exception as e:
            print(e)
            connection.conn.rollback()
        else:
            print("Password changed!")
            connection.conn.commit()

    @staticmethod
    def fetch_all_operators(connection):
        with connection.conn:
            cursor = connection.conn.cursor()
            query = cursor.execute("select OperatorID, Op_username, Op_shift, Op_role from dbo.operators")
            operators = [row for row in query]
        return operators

    @staticmethod
    def fetch_operator_shift(connection, operator):
        cursor = connection.conn.cursor()
        shifts = cursor.execute("select Op_shift from dbo.operators where Op_username = ?", operator)
        return [item.Op_shift for item in shifts]        

    @staticmethod
    def check_password(connection, operator, password):
        cursor = connection.conn.cursor()
        try:
            entry = cursor.execute("select * from dbo.operators where Op_username = ?", operator)
            for item in entry:
                if item.Op_PW != None:
                    pw = item.Op_PW
        except Exception as e:
            print(e)
        else:
            if Operator._get_hash(password) == pw:
                return True
            else:
                return False